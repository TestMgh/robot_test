*** Settings ***
Resource    common_settings.robot

*** Keywords ***
Open Application
    Open browser    ${URL}    ${BROWSER}

Login Page Should Be Open
    Title Should Be    ALEX

Go To Login Page
    Go To    ${URL}
    Login Page Should Be Open

Go To Home Page
    Go To    ${ALEX_HOME}
    Sleep    ${DELAY}

Go To Favourites
    Wait Until Page Contains Element     xpath=//*[@id="appbar-wrapper"]/div/div[1]/div[3]/div[2]/button
    Click Element  xpath=//*[@id="appbar-wrapper"]/div/div[1]/div[3]/div[2]/button
    Wait Until Page Contains Element    xpath=/html/body/div[23]/div//*[text()="Favourites"]/parent::div
    Click Element  xpath=/html/body/div[23]/div//*[text()="Favourites"]/parent::div

Input Username
    [Arguments]    ${username}
    Input Text    username    ${username}

Continue
    Click Button    submit

Input Password
    [Arguments]    ${password}
    Input Text    password    ${password}

Submit Credentials
    Click Button    submit

Welcome Page Should Be Open
    Wait Until Page Contains Element    id:searchAutoComplete
    Location Should Be    ${ALEX_HOME}
    Title Should Be    ALEX

Log Out
    Go To    ${ALEX_LOGOUT}

Close Application
    Close All Browsers

Select The First Node
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
    Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input

Select First Two Nodes
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[1]/input
    Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
    Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[1]/input

# Tabs clicking
Click Explore Tab
    Click Element    xpath: //button[.//text() = 'Explore']

Click Capture Tab
    Click Element    xpath: //button[.//text() = 'Capture']

# Search nodes based on business names
Search For Assets
    [Arguments]    ${search_term}
    Input Text    id:searchAutoComplete    ${search_term}
    Click Element    xpath: //*[@id="appbar-wrapper"]/div/div[1]/div[2]/div[2]/button[1]

Node With This Business Name Returned
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr/td[6][contains(text(),'${BUS_NAME_SEARCH_TERM}')]
    Element Text Should Be    xpath=//*[@id="nodesTable"]/tbody/tr/td[6]    ${BUS_NAME_SEARCH_TERM}

# Facet filter
Expand Filter
    [Arguments]    ${filter_type}
    Wait Until Page Contains Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]/div/div/div[2]//div[text()='${filter_type}']/following-sibling::button
    Click Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]/div/div/div[2]//div[text()='${filter_type}']/following-sibling::button

Apply Filter
    [Arguments]    ${filter_type}   ${filter}
    Wait Until Page Contains Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]//*/div[text()='${filter_type}']/parent::div/following-sibling::div//label[contains(text(),'${filter}')]/parent::div/parent::div
    Click Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]//*/div[text()='${filter_type}']/parent::div/following-sibling::div//label[contains(text(),'${filter}')]/parent::div/parent::div

Nodes With The Specific Filter Returned
    [Arguments]    ${filter}    ${column_num}
    ${Rows}    Get Element Count    //*[@id="nodesTable"]/tbody/tr
    :FOR    ${row}    IN RANGE    1    ${Rows}
    \    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[${row}]/td[${column_num}][contains(text(),'${filter}')]

Nulls Nodes Should Be Returned
    ${Rows}    Get Element Count    //*[@id="nodesTable"]/tbody/tr
    :FOR    ${row}    IN RANGE    1    ${Rows}
    \    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[${row}]/td[13][contains(text(),'')]


Go To Map View
     Wait Until Page Contains Element    xpath=//*[@id="tab-content-switch-bar"]/div/div[1]//div[contains(text(),"Map")]
     Maximize Browser Window
     Click Element   xpath=//*[@id="tab-content-switch-bar"]/div/div[1]//div[contains(text(),"Map")]/parent::div

Show Hidden
    Wait Until Page Contains Element    xpath=//*[@id="graphExplorerControl"]/div/div[5]/button
    Click Button    xpath=//*[@id="graphExplorerControl"]/div/div[5]/button
    Wait Until Page Contains Element     xpath=/html/body//*[contains(text(),'Show Hidden')]/parent::div/parent::div
    Click Element    xpath=/html/body//*[contains(text(),'Show Hidden')]/parent::div/parent::div

Show All
    Click Element   xpath=//*[@id="graphExplorerControl"]/div//*[text()='Simplify Graph']/parent::div//*/button
    Click Element   xpath=/html/body//*/span[@label='Show all']

# Neighbours
Check If Neighbours Of Single Node Are Returned
     Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
     Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
     ${node_asset_type}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
     ${node_asset_name}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
     Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
     Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
     Click Link    xpath=//*[@id="list-bulk-update-control"]//a[text()='Neighbours']
     Click Button    xpath=/html/body/div[11]/div/div[1]/div/div/div[2]/button[2]
     Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr
     ${child_list}   Fetch Relationships    ${node_asset_name}
     Go To Map View
     Show Hidden
     Show All
     Go To Map View
     Check For Neighbouring Nodes     ${node_asset_name}    ${child_list}

Check If Neighbours Of Multiple Nodes Are Returned
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[2]
    ${node_asset_type_1}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
    ${node_asset_name_1}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
    ${node_asset_type_2}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[5]
    ${node_asset_name_2}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[6]
    Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
    Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[1]/input
    Click Link    xpath=//*[@id="list-bulk-update-control"]//a[text()='Neighbours']
    Click Button    xpath=/html/body/div[11]/div/div[1]/div/div/div[2]/button[2]
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr
    ${list_node_1}     Fetch Relationships     ${node_asset_name_1}
    ${list_node_2}     Fetch Relationships     ${node_asset_name_2}
    Go To Map View
    Show Hidden
    Show All
    Check For Neighbouring Nodes    ${node_asset_name_1}    ${list_node_1}
    Check For Neighbouring Nodes    ${node_asset_name_2}    ${list_node_2}

Check For Neighbouring Nodes
    [Arguments]     ${parent_node}   ${child_node_list}
    Wait Until Page Contains Element   //*[@id="graph-view-svg"]//*[text()="${parent_node}"]
    ${node_coordinate}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${parent_node}"]/parent::*    transform
    ${node_coordinate}  Fetch From Left    ${node_coordinate}    )
    ${node_coordinate}  Fetch From Right    ${node_coordinate}   (
    :FOR    ${element}   In     @{child_node_list}
    \   ${child_coordinate}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${element}"]/parent::*    transform
    \   ${child_coordinate}  Fetch From Left    ${child_coordinate}    )
    \   ${child_coordinate}  Fetch From Right    ${child_coordinate}   (
    \   ${count1} =     Get Element Count   xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${child_coordinate},${node_coordinate}')]
    \   Run Keyword If     ${count1}==0    Page Should Contain Element    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${node_coordinate},${child_coordinate}')]

Fetch Relationships
    [Arguments]     ${business_name}
    Click Element    xpath=//*[@id="nodesTable"]/tbody/tr//*[text()='${business_name}']
    Click Element    xpath=//*[@id="nodeNeighbours"]//*/a[text()="Relationships"]
    @{list}     Create List
    ${Rows}     Get Element Count    //*[@id="nodeNeighbours"]/div/div[contains(@class,'node-neighbours')]/div[1]
    :FOR    ${row}    IN RANGE    0    ${Rows}
    \  ${col_name}   Get Text    //*[@id="nodeNeighbours"]/div/div[${row}+2]/div[1]
    \  ${col_name}    Run Keyword If    '${col_name}'=='This'   Get Text    //*[@id="nodeNeighbours"]/div/div[${row}+2]/div[2]   ELSE   Set Variable  ${col_name}
    \   Append To List     ${list}      ${col_name}
    Wait Until Element Is Visible  xpath=//*[@id="Main-section"]
    Set Selenium Speed    0.4 second
    Mouse Over    xpath=//*[@class="action-bar-close"]
    Click Element  xpath=//*[@class="action-bar-close"]
    [Return]     ${list}


Return Marked Favourite
    ${business_name}  Get Text  //*[@id="nodesTable"]/tbody/tr[1]/td[6]
    [Return]  ${business_name}

Mark Favourite
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[2]/img
    Click Element  xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[2]/img

Check If Node Added To Favourites
    [Arguments]  ${business_name}
    Go To Favourites
    Wait Until Page Contains Element  xpath=//*[@id="subscriptionwrapper"]//*[text()='${business_name}']

Execute Favourite
    [Arguments]  ${business_name}
    Click Element   xpath=//*[@id="subscriptionwrapper"]//*[text()='${business_name}']
    Wait Until Page Contains Element   xpath=//*[@id="details-view"]
    Page Should Contain Element     xpath=//*[@id="details-view"]//*/input[@value='${business_name}']
    Click Element   xpath=//*[@id="details-view"]//*/a[@title='Subscribe']

Remove From Favourites
    Click Element  xpath=//*[@id="subscriptionwrapper"]/div/div[2]/div/div/div[2]/span/img

# Download import manager files
Download Import-Manager Files From The First Job
    Wait Until Page Contains Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
    ${date}    Get Current Date    result_format=epoch
#    ${date_epoch_aux}    Convert To Number    ${date}    precision=1
    ${date_epoch}    Convert To Number    ${date}    precision=0
    ${date_epoch_string}    Convert To String    ${date_epoch}
    ${file_name}    Get Substring    ${date_epoch_string}    0    8
    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[text()="Download"]/parent::div
    Wait Until Created    ${DOWNLOAD_LOCATION}/${file_name}[0-9][0-9].zip
    File Should Exist    ${DOWNLOAD_LOCATION}/${file_name}[0-9][0-9].zip

Import Data Assets To Alex
    ${input_files}    Set Variable    @{LIST_OF_FILES}[0]
    ${list_length}    Get Length    ${LIST_OF_FILES}
    :FOR    ${index}    IN RANGE    1    ${list_length}
    \    ${input_files}    Catenate    SEPARATOR= \n     ${input_files}    @{LIST_OF_FILES}[${index}]
    Wait Until Page Contains Element    //*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[2]/div/label/div/div/input
    Choose Files    //*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[2]/div/label/div/div/input    ${input_files}
    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[contains(text(),'Promote')]
    :FOR    ${i}    IN RANGE    3
    \    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[contains(text(),'Refresh')]

Verify Dataset Upload
    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[text()="Show Stats"]/parent::div
    Wait Until Page Contains Element    xpath=/html/body/div[9]/div/div[1]/div/div//*[@class="pending-stats"]
    ${number_of_assets}    Get Text    xpath=/html/body/div[9]/div/div[1]/div/div//*[@class="pending-stats"]/div[1]
    Should Be Equal As Strings    ${number_of_assets}    3
    Go To Home Page
    Expand Filter     ${TECHNOLOGY}
    Click Explore Tab
    Wait Until Page Contains Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]/div/div/div[2]//div[text()='Technology']/parent::div/following-sibling::div//label[contains(text(),'SampleTechnology')]
    Click Element    xpath=//*[@id="appbar-wrapper"]/div/div[3]/div/div/div[2]//div[text()='Technology']/parent::div/following-sibling::div//label[contains(text(),'SampleTechnology')]/parent::div/parent::div
    Wait Until Page Contains Element    //*[@id="nodesTable"]/tbody/tr[1]//td[12][text()="SampleTechnology"]
    Page Should Contain Element    //*[@id="nodesTable"]/tbody/tr[2]//td[12][text()="SampleTechnology"]
    Page Should Contain Element    //*[@id="nodesTable"]/tbody/tr[3]//td[12][text()="SampleTechnology"]
    Click Capture Tab
    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[2]/table/tbody/tr[1]
    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[contains(text(),'Rollback')]
    Click Element    xpath=//*[@id="job-history-container"]/div/div/div/div[1]/table/thead/tr[1]/th/div/div[1]//*[contains(text(),'Refresh')]

#The keyword looks for the argument to be present in one of the columns in the table.
#If not then it opens the enrich page and looks for all the property values for the searched string.
Check If Nodes Returned Have Searched Property
    [Arguments]    ${search_term}
    ${search_term} =  Convert To Uppercase  ${search_term}
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]
    ${present} =  Get Element Count     xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[contains(translate(text(),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'), '${search_term}')]
    Click Element  xpath=//*[@id="nodesTable"]/tbody/tr/td[5]
    Wait Until Page Contains Element    xpath=//*[@id="nodeDetailForm"]//*[@id='Main-section']
    ${present} =  Run Keyword If  ${present}==0    Get Element Count  xpath=//*[@id="nodeDetail"]//*/input[contains(translate(@value,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'), '${search_term}')]
    ${present} =  Run Keyword If  ${present}==0    Get Element Count   xpath=//*[@id="nodeDetail"]//*/textarea[contains(translate(@value,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'), '${search_term}')]
    Run Keyword If  ${present}==0   Wait Until Page Contains Element  xpath=//*[@id="nodeNeighbours"]//*[contains(translate(text(),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'), '${search_term}')]
    #Checking dropdowns remaining
    Set Selenium Speed    0.4 second
    Click Element  xpath=//*[@class="action-bar-close"]

Check If Business Name Contains String
    [Arguments]     ${search_term}
    ${search_term} =    Convert To Uppercase    ${search_term}
    ${Rows}    Get Element Count    //*[@id="nodesTable"]/tbody/tr
    :FOR    ${row}    IN RANGE    1    ${Rows}
    \    ${business_name} =  GET TEXT  xpath=//*[@id="nodesTable"]/tbody/tr[${row}]/td[6]
    \    ${business_name} =  Convert To Uppercase   ${business_name}
    \    Should Contain  ${business_name}  ${search_term}

Go To Advanced Search
    Click Element  xpath=//*[@id="appbar-wrapper"]/div/div[1]/div[2]/div[2]/button[2]

Populate Values In Asset Property Advanced Search
    [Arguments]    ${asset_property}  ${asset_property_val}
    Input Text    xpath=//html/body/div[23]//*[text()="Select Asset Property"]/parent::div/child::input  ${asset_property}
    Input Text    xpath=//html/body/div[23]//*[text()="Asset Property Value"]/parent::div/child::input   ${asset_property_val}

Save Search
    [Arguments]   ${saved_as}
    Click Element  xpath=/html/body/div[23]//*[text()="Save Search"]/parent::div
    Input Text  xpath=/html/body/div[24]//*[text()="Search Name"]/parent::div/child::input  ${saved_as}
    Click Element  xpath=/html/body/div[24]/div/div[1]/div/div/div[2]/button[2]

Check For Saved Search
    [Arguments]   ${saved_as}
    Set Selenium Speed    0.4 second
    Wait Until Page Contains Element  xpath=//*[@id="favouriteQuerieswrapper"]//*[text()="${saved_as}"]

Delete Saved Search
    [Arguments]   ${saved_as}
    Set Selenium Speed    0.4 second
    Click Element  xpath=//*[@id="favouriteQuerieswrapper"]//*[text()="${saved_as}"]/parent::div/parent::div/div/i

# Keep-only on single selection
Check If Only Nodes Of Subcontext Remain
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable_ellipsis"]/following-sibling::li/a
    ${num_of_pages}    Get Text    xpath=//*[@id="nodesTable_ellipsis"]/following-sibling::li/a
    Expand Filter    ${TECHNOLOGY}
    Apply Filter    ${TECHNOLOGY}    ${TECHNOLOGY_FILTER}
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable_ellipsis"]/following-sibling::li/a
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable_ellipsis"]/following-sibling::li/a[text()="${NUM_OF_PAGES_ORACLE}"]
    ${results}    Get Text    xpath=//*[@id="HomeAppBarSearchResult"]//*[contains(@class, 'resultSize')]
    ${num_of_assets}    Fetch From Left    ${results}    of
    # select all
    Click Element    xpath=//*[@id="tab-list-switchs"]//div[@id="list-bulk-update-selectAll"]/input
    # select End-to-End
    Click Element    xpath=//*[@id="tab-list-switchs"]//a[text()="Keep Only"]
    Wait Until Page Contains Element    xpath=//*[@id="HomeAppBarSearchResult"]//*[contains(@class,"resultSize")][text()="${num_of_assets.strip()}"]

# Hide on node selection
Check If The Number Of Nodes Reduces By 1
     Wait Until Page Contains Element    xpath=//*[@id="nodesTable_ellipsis"]/following-sibling::li/a
     ${num_of_pages}    Get Text    xpath=//*[@id="nodesTable_ellipsis"]/following-sibling::li/a
     Expand Filter    ${TECHNOLOGY}
     Apply Filter    ${TECHNOLOGY}    ${TECHNOLOGY_FILTER}
     Wait Until Page Contains Element    xpath=//*[@id="nodesTable_ellipsis"]/following-sibling::li/a
     Wait Until Page Contains Element    xpath=//*[@id="nodesTable_ellipsis"]/following-sibling::li/a[text()="${NUM_OF_PAGES_ORACLE}"]
     ${results}    Get Text    xpath=//*[@id="HomeAppBarSearchResult"]//*[contains(@class, 'resultSize')]
     ${num_of_assets}    Fetch From Left    ${results}    of
     ${num_of_assets}    Convert To Integer    ${num_of_assets.strip()}
     ${num_of_assets}    Convert To String    ${num_of_assets-1}
     # select the first asset
     Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[1]/input
     # select Hide
     Click Element    xpath=//*[@id="tab-list-switchs"]//a[text()="Hide"]
     Wait Until Page Contains Element    xpath=//*[@id="HomeAppBarSearchResult"]//*[contains(@class,"resultSize")][text()="${num_of_assets}"]

# Impact on node selection
Check Impact Of Data Asset
    Click Element    xpath=//*[@id="tab-list-switchs"]//a[text()="Impact"]

Check If Impact Results For Single Node Are Returned
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[6]
    ${node_asset_type}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
    ${node_asset_name}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
    ${impact_node_name}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[6]
    ${impact_node_type}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[5]
    Maximize Browser Window
    Click Element   xpath=//*[@id="tab-content-switch-bar"]/div/div[1]//div[contains(text(),"Map")]/parent::div
    # show hidden
    Wait Until Page Contains Element    xpath=//*[@id="graphExplorerControl"]/div/div[5]/button
    Click Button    xpath=//*[@id="graphExplorerControl"]/div/div[5]/button
    Click Element    xpath=/html/body/div[23]/div/div/div/div/div//*[contains(text(),'Show Hidden')]/parent::div/parent::div
    Wait Until Page Contains Element    xpath=//*[@id="graph-view-svg"]//*[text()="${node_asset_type}"]
    # expand selected node
    Click Element    xpath=//*[@id="graph-view-svg"]//*[text()="${node_asset_type}"]
    Click Element    xpath=//*[@id="graphModal"]//*[text()="Expand"]
    Wait Until Page Contains Element    xpath=//*[@id="graph-view-svg"]//*[text()="${node_asset_name}"]
    # expand impact node
    Click Element    xpath=//*[@id="graph-view-svg"]//*[text()="${impact_node_type}"]
    Click Element    xpath=//*[@id="graphModal"]//*[text()="Expand"]
    Wait Until Page Contains Element    xpath=//*[@id="graph-view-svg"]//*[text()="${impact_node_name}"]
    ${node_coordinate}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${node_asset_name}"]/parent::*    transform
    ${impact_node_coordinate}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${impact_node_name}"]/parent::*    transform
    ${node_coordinate}  Fetch From Left    ${node_coordinate}    )
    ${node_coordinate}  Fetch From Right    ${node_coordinate}    (
    ${impact_node_coordinate}  Fetch From Left    ${impact_node_coordinate}    )
    ${impact_node_coordinate}  Fetch From Right    ${impact_node_coordinate}    (
    ${count} =     Get Element Count   xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${impact_node_coordinate},${node_coordinate}')]
    Run Keyword If     ${count}==0    Page Should Contain Element    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${node_coordinate},${impact_node_coordinate}')]
    ...    ELSE    Page Should Contain Element    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${impact_node_coordinate},${node_coordinate}')]

Check If Impact Results For Multiple Nodes Are Returned
    # sort based on the type of asset
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable_wrapper"]/div[1]/div[1]/div/table/thead/tr/th[5]
    Click Element    xpath=//*[@id="nodesTable_wrapper"]/div[1]/div[1]/div/table/thead/tr/th[5]
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
    ${node1_asset_type}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
    ${node1_asset_name}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
    ${node2_asset_type}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[5]
    ${node2_asset_name}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[2]/td[6]
    ${num_of_nodes}    Get Element Count    xpath=//*[@id="nodesTable"]/tbody/tr
    @{name_list}    Create List
    # populate type_list
    :FOR    ${row}    IN RANGE    3    ${num_of_nodes+1}
    \    ${tmp_asset_name}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[${row}]/td[6]
    \    Append To List    ${name_list}    ${tmp_asset_name}
    Log Variables
    Maximize Browser Window
    Click Element   xpath=//*[@id="tab-content-switch-bar"]/div/div[1]//div[contains(text(),"Map")]/parent::div
    # show hidden
    Wait Until Page Contains Element    xpath=//*[@id="graphExplorerControl"]/div/div[5]/button
    Click Button    xpath=//*[@id="graphExplorerControl"]/div/div[5]/button
    Click Element    xpath=/html/body/div[23]/div/div/div/div/div//*[contains(text(),'Show Hidden')]/parent::div/parent::div
    # show all
    Wait Until Page Contains Element    xpath=//*[@id="graphExplorerControl"]/div/div[2]//*[text()="Group by property"]/parent::div/button
    Click Button    xpath=//*[@id="graphExplorerControl"]/div/div[2]//*[text()="Group by property"]/parent::div/button
    Wait Until Page Contains Element    xpath=/html/body/div[23]/div//div[text()="Show all"]
    Click Element    xpath=/html/body/div[23]/div//div[text()="Show all"]
    # node1 coordinate
    Wait Until Page Contains Element    xpath=//*[@id="graph-view-svg"]//*[text()="${node1_asset_name}"]/parent::*
    ${node1_coordinate}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${node1_asset_name}"]/parent::*    transform
    ${node1_coordinate}    Fetch From Left    ${node1_coordinate}    )
    ${node1_coordinate}    Fetch From Right    ${node1_coordinate}   (
    # node2 coordinate
    ${node2_coordinate}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${node2_asset_name}"]/parent::*    transform
    ${node2_coordinate}    Fetch From Left    ${node2_coordinate}    )
    ${node2_coordinate}    Fetch From Right    ${node2_coordinate}   (
    # create coordinate_list
    @{count_list_1}    Create List
    @{count_list_2}    Create List
    :FOR    ${i}    IN RANGE    0    ${num_of_nodes-2}
    \    ${tmp_node_coordinate}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="@{name_list}[${i}]"]/parent::*    transform
    \    ${tmp_node_coordinate}    Fetch From Left    ${tmp_node_coordinate}    )
    \    ${tmp_node_coordinate}    Fetch From Right    ${tmp_node_coordinate}    (
    \    ${count1}    Get Element Count    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${tmp_node_coordinate},${node1_coordinate}')]
    \    ${count2}    Get Element Count    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${node1_coordinate},${tmp_node_coordinate}')]
    \    ${count3}    Get Element Count    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${tmp_node_coordinate},${node2_coordinate}')]
    \    ${count4}    Get Element Count    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${node2_coordinate},${tmp_node_coordinate}')]
    \    Append To List    ${count_list_1}    ${count1}
    \    Append To List    ${count_list_1}    ${count2}
    \    Append To List    ${count_list_2}    ${count3}
    \    Append To List    ${count_list_2}    ${count4}
    Log Variables
    List Should Contain Value    ${count_list_1}    ${2}
    List Should Contain Value    ${count_list_2}    ${2}

# Lineage on node selection
Check Lineage Of Data Asset
    Click Element    xpath=//*[@id="tab-list-switchs"]//a[text()="Lineage"]

Check If Results Of Lineage Are Returned
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
    ${node_asset_type}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[5]
    ${node_asset_name}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr[1]/td[6]
    # open enrich page of the first node
    Click Element    xpath=//*[@id="nodesTable"]/tbody/tr[1]
    Wait Until Element Is Visible    xpath=//*[@id="details-view"]
    Click Element    xpath=//*[@id="nodeNeighbours"]/legend/a
    ${lineage_node_name}    Get Text    xpath=//*[@id="nodeNeighbours"]/div/div[2]/div[1]
    # close enrich page
    Reload Page
    Wait Until Page Contains Element    xpath=//*[@id="nodesTable"]/tbody/tr
    ${lineage_node_type}    Get Text    xpath=//*[@id="nodesTable"]/tbody/tr/td[6][text()="${lineage_node_name}"]/parent::tr/td[5]
    Maximize Browser Window
    Click Element   xpath=//*[@id="tab-content-switch-bar"]/div/div[1]//div[contains(text(),"Map")]/parent::div
    # show hidden
    Wait Until Page Contains Element    xpath=//*[@id="graphExplorerControl"]/div/div[5]/button
    Click Button    xpath=//*[@id="graphExplorerControl"]/div/div[5]/button
    Click Element    xpath=/html/body/div[23]/div/div/div/div/div//*[contains(text(),'Show Hidden')]/parent::div/parent::div
    Wait Until Page Contains Element    xpath=//*[@id="graph-view-svg"]//*[text()="${node_asset_type}"]
    # expand selected node
    Click Element    xpath=//*[@id="graph-view-svg"]//*[text()="${node_asset_type}"]
    Click Element    xpath=//*[@id="graphModal"]//*[text()="Expand"]
    Wait Until Page Contains Element    xpath=//*[@id="graph-view-svg"]//*[text()="${node_asset_name}"]
    # expand lineage node
    Click Element    xpath=//*[@id="graph-view-svg"]//*[text()="${lineage_node_type}"]
    Click Element    xpath=//*[@id="graphModal"]//*[text()="Expand"]
    Wait Until Page Contains Element    xpath=//*[@id="graph-view-svg"]//*[text()="${lineage_node_name}"]
    ${node_coordinate}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${node_asset_name}"]/parent::*    transform
    ${lineage_node_coordinate}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()="${lineage_node_name}"]/parent::*    transform
    ${node_coordinate}  Fetch From Left    ${node_coordinate}    )
    ${node_coordinate}  Fetch From Right    ${node_coordinate}   (
    ${lineage_node_coordinate}  Fetch From Left    ${lineage_node_coordinate}    )
    ${lineage_node_coordinate}  Fetch From Right    ${lineage_node_coordinate}   (
    ${count} =     Get Element Count   xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${lineage_node_coordinate},${node_coordinate}')]
    Run Keyword If     ${count}==0    Page Should Contain Element    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${node_coordinate},${lineage_node_coordinate}')]
    ...    ELSE    Page Should Contain Element    xpath=//*[@id="graph-view-svg"]//*[contains(@d,'M${lineage_node_coordinate},${node_coordinate}')]

Get All Asset Types
    @{asset_types}     Create List
    ${num_of_assets}    Get Element Count  xpath=//*[@id="appbar-wrapper"]/div/div[3]/div/div/div[2]//div[text()='Type of Asset']/parent::div/parent::div//*/label[text()!='All']
    :For    ${row}  In Range   0    ${num_of_assets}
#     \  ${asset_name}       Get Text    xpath=//*[@id="appbar-wrapper"]/*//div[text()='Type of Asset']/parent::div/parent::div//*/label[text()!='All']
     \  ${asset_name}       Get Text    xpath=//*[@id="appbar-wrapper"]/*//div[text()='Type of Asset']/parent::div/parent::div/div/div[${row+2}]/*//label
     \  ${asset_name}       Fetch From Left     ${asset_name}   (
     \  ${asset_name}       Strip String     ${asset_name}
     \   Append To List     ${asset_types}  ${asset_name}
     [Return]   ${asset_types}

Get All Nodes On Map
     @{nodes}       Create List
     ${num_of nodes}    Get Element Count   //*[@id="graph-view-svg"]//*/*[@class='node']/child::*
     :For    ${row}  In Range   0    ${num_of_nodes}
     \  Wait Until Page Contains Element    //*[@id="graph-view-svg"]//*/*[@class='node']/*[${row}+1]/*[@class='circle-text'][normalize-space(text()) != ""]
     \  ${node}     Get Text    //*[@id="graph-view-svg"]//*/*[@class='node']/*[${row}+1]/*[@class='circle-text'][normalize-space(text()) != ""]
     \  Append To List      ${nodes}    ${node}
     [Return]   ${nodes}


Check For Nodes
    ${asset_types}  Get All Asset Types
    ${nodes}    Get All Nodes On Map
    :For    ${node}    IN  @{nodes}
    \   Wait Until Page Contains Element     //*[@id="graph-view-svg"]//*[text()='${node}']
    \   ${node_coordinate}    Get Element Attribute    //*[@id="graph-view-svg"]//*[text()='${node}']/parent::*    transform
    \   ${node_coordinate}  Fetch From Left    ${node_coordinate}    )
    \   ${node_coordinate}  Fetch From Right    ${node_coordinate}   (
    \   Wait Until Page Contains Element   xpath=//*[@id="graph-view-svg"]//*[contains(@d,'${node_coordinate}')]


Map Should Only Contain
    [Arguments]     ${filter1}  ${filter2}
    Wait Until Page Contains Element    //*[@id="graph-view-svg"]//*/*[@class='node']
    ${num_of_nodes}    Get Element Count   //*[@id="graph-view-svg"]//*/*[@class='node']/child::*
    :For    ${row}  In Range   0    ${num_of_nodes}
    \  ${node}     Get Text    //*[@id="graph-view-svg"]//*/*[@class='node']/*[${row}+1]/*[@class='circle-text']
    \  Run Keyword If   '${node}'!='${filter1}'     Should Be Equal   ${node}   ${filter2}
