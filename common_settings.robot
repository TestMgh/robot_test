*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library  DateTime
Library  String
Library  Collections
Library  customized_keywords/MultipleFilesUpload.py


*** Variables ***
${URL}          https://release-test.alex4im.com
${BROWSER}      Chrome
${USERNAME}     admin
${PASSWORD}     Alexatlas123
${DELAY}        23 seconds
${ALEX_HOME}    ${URL}/home
${ALEX_LOGOUT}    ${URL}/logout
${BUS_NAME_SEARCH_TERM}    DEQ_TID
${TECHNOLOGY}       Technology
${TECHNOLOGY_COL_NO}    12
${TECHNOLOGY_FILTER}    Oracle
${TYPE_OF_ASSET}    Type of Asset
${DATA_FLOW}    Data Flow
${DATA_FLOW_COL_NO}    13
${ASSET_COL_NO}     5
${ASSET_FILTER}     Table
${ASSET_FILTER_2}   Column
${DOWNLOAD_LOCATION}    /Users/lukesteyn/Downloads
${NODE_IMPORT}    /Users/lukesteyn/Desktop/test_data_upload/User1_Nodes.csv
${NODE_RELATIONSHIP_IMPORT}    /Users/lukesteyn/Desktop/test_data_upload/User1_Relationships.csv
${TAG_IMPORT}    /Users/lukesteyn/Desktop/test_data_upload/Harley_Tag_File.properties
@{LIST__OF_FILES}    ${NODE_IMPORT}    ${NODE_RELATIONSHIP_IMPORT}    ${TAG_IMPORT}
${PROPERTY_SEARCH_TERM}  table
${BUS_NAME_SUBSTR}  Cust
${ASSET_PROPERTY}  Business name
${ASSET_PROPERTY_VALUE}  Customer
${NEIGHBOUR_FOR_NODE}   gw_mdfa_prty_add
${NUM_OF_PAGES_ORACLE}    127
